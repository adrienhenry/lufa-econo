// ==UserScript==
// @name        lufa écono
// @description Commande automatique du sac écono
// @version     2
// @updateURL   https://bitbucket.org/adrienhenry/lufa-econo/src/master/lufa-econo.user.js
// @include     https://montreal.lufa.com/fr/superMarket/search/favorites
// @icon        https://bytebucket.org/adrienhenry/lufa-econo/raw/master/logo_lufa-econo.svg
// ==/UserScript==


var refresh = 60;//second
var addToBasket = ["Sac Écono","Pas beaux, mais délicieux","Légumes imparfaits"];
var numToAdd = [1,1,1];
var regex = [];
for(var i=0;i<addToBasket.length;i++){
    rr = new RegExp(addToBasket[i],"i");
    regex.push(rr);
}



$(document).ready(function() {
    document.body.style.background = "url('https://bytebucket.org/adrienhenry/lufa-econo/raw/master/background.jpg')";
    document.getElementById("mobile-header").style.background = "#090649";
    
    document.getElementsByClassName("mobile-logo")[0].style.background = "url('https://bytebucket.org/adrienhenry/lufa-econo/raw/master/logo_lufa-econo.svg')"
    document.getElementsByClassName("mobile-logo")[0].style.backgroundSize = "100px"
    
    products = document.getElementsByClassName("single-product-wrapper product-wrapper-favorites");
    for(var i=0;i< products.length;i++){
        var prod = products[i];
        var productName = products[i].getElementsByClassName("product-name")[0].textContent.match(/\w.*\w\)?/)[0];
        var matchProduct = 0;
        var index = -1;
        for (var j=0;j<regex.length;j++){
            matchProduct = productName.match(regex[j])!=null;

            if(matchProduct){
                index = j;
                break;
            }
        }
        
        if(matchProduct){
            console.log(productName);
            console.log(numToAdd[index]);
            var buySection = products[i].getElementsByClassName("buy-buttons")[0];
            var marketOpen = buySection!=null;
            if(marketOpen){
		var nbItems = parseInt(products[i].getElementsByClassName("number-of-items")[0].firstChild.getAttribute("value"));
		if(nbItems<numToAdd[index]){products[i].getElementsByClassName("plus")[0].click();}
		if(nbItems>numToAdd[index]){products[i].getElementsByClassName("minus")[0].click();}
            }
        }
    }
    setTimeout(function(){
	window.location.reload(1);
    }, refresh*1000);
});