# Lufa: Sac Écono #

Ce script continue de charger la page Fruits des paniers lufa jusqu'à l'ouverture du marché afin de réserver son "sac écono" au plus vite.


# Installation avec Firefox
### Installer le plugin [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) s'il n'est pas déja installé.![alt text](https://addons.cdn.mozilla.net/user-media/addon_icons/0/748-64.png)

### Installer le script [lufa-econo](https://bitbucket.org/adrienhenry/lufa-econo/raw/master/lufa-econo.user.js)
Pour installer une mise à jour, il faudra recliquer sur le lien.

# Démarrer le script

### S'assurer que l'ordinateur n'ira pas en veille pendant que le scipt tourne.

### S'assurer que le plugin Greasemonky est "enabled" (Le bouton disable est visible).

- Taper "about:addons" dans la barre de recherche firefox.

- Aller dans la rubrique "Extensions"

### S'assurer que le script lufa-écono est "enabled" (Le bouton disable est visible).

- Taper "about:addons" dans la barre de recherche firefox.

- Aller dans la rubrique "User scripts"

- Lorsqu'on ne désire pas que le script tourne, il suffit de le désactiver à cet endroit.

### Se rendre sur la page lufa et se connecter à son compte

### Aller sur la page [favoris](https://montreal.lufa.com/fr/superMarket/search/favorites) du marché.
Il faut s'assurer que le panier écono fasse partie des favoris
# Le script tourne lorsque la page prend l'allure ci-dessous:
![Screenshot-lufa.png](https://bitbucket.org/repo/oddd6g/images/386133668-Screenshot-lufa.png)